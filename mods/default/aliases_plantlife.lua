-- mods/default/aliases_plantlife.lua

-- MASSIVE overwrite to old nodes and items merged into default


--B U S H E S--
minetest.register_alias("bushes:youngtree2_bottom", "default:youngtree2_bottom")
minetest.register_alias("bushes:bushbranches1", "default:bushbranches1")
minetest.register_alias("bushes:bushbranches2", "default:bushbranches2")
minetest.register_alias("bushes:bushbranches3", "default:bushbranches3")
minetest.register_alias("bushes:bushbranches4", "default:bushbranches4")
minetest.register_alias("bushes:BushLeaves1", "default:BushLeaves1")
minetest.register_alias("bushes:BushLeaves2", "default:BushLeaves2")
minetest.register_alias("bushes:BushLeaves3", "default:BushLeaves3")
minetest.register_alias("bushes:BushLeaves4", "default:BushLeaves4")

--B U S H E S   C L A S S I C--
minetest.register_alias("bushes:blueberry", "default:blueberry")
minetest.register_alias("bushes:blackberry", "default:blackberry")
minetest.register_alias("bushes:raspberry", "default:raspberry")
minetest.register_alias("bushes:strawberry", "default:strawberry")
minetest.register_alias("bushes:gooseberry", "default:gooseberry")
minetest.register_alias("bushes:mixed_berry", "default:mixed_berry")

minetest.register_alias("bushes:fruitless_bush", "default:fruitless_bush")
minetest.register_alias("bushes:blueberry_bush", "default:blueberry_bush")
minetest.register_alias("bushes:blackberry_bush", "default:blackberry_bush")
minetest.register_alias("bushes:raspberry_bush", "default:raspberry_bush")
minetest.register_alias("bushes:strawberry_bush", "default:strawberry_bush")
minetest.register_alias("bushes:gooseberry_bush", "default:gooseberry_bush")
minetest.register_alias("bushes:mixed_berry_bush", "default:mixed_berry_bush")

minetest.register_alias("bushes:basket_blueberry", "default:basket_blueberry")
minetest.register_alias("bushes:basket_blackberry", "default:basket_blackberry")
minetest.register_alias("bushes:basket_raspberry", "default:basket_raspberry")
minetest.register_alias("bushes:basket_strawberry", "default:basket_strawberry")
minetest.register_alias("bushes:basket_gooseberry", "default:basket_gooseberry")
minetest.register_alias("bushes:basket_mixed_berry", "default:basket_mixed_berry")

minetest.register_alias("bushes:blueberry_pie_raw", "default:blueberry_pie_raw")
minetest.register_alias("bushes:blackberry_pie_raw", "default:blackberry_pie_raw")
minetest.register_alias("bushes:raspberry_pie_raw", "default:raspberry_pie_raw")
minetest.register_alias("bushes:strawberry_pie_raw", "default:strawberry_pie_raw")
minetest.register_alias("bushes:gooseberry_pie_raw", "default:gooseberry_pie_raw")
minetest.register_alias("bushes:mixed_berry_pie_raw", "default:mixed_berry_pie_raw")

minetest.register_alias("bushes:blueberry_pie_cooked", "default:blueberry_pie_cooked")
minetest.register_alias("bushes:blackberry_pie_cooked", "default:blackberry_pie_cooked")
minetest.register_alias("bushes:raspberry_pie_cooked", "default:raspberry_pie_cooked")
minetest.register_alias("bushes:strawberry_pie_cooked", "default:strawberry_pie_cooked")
minetest.register_alias("bushes:gooseberry_pie_cooked", "default:gooseberry_pie_cooked")
minetest.register_alias("bushes:mixed_berry_pie_cooked", "default:mixed_berry_pie_cooked")

minetest.register_alias("bushes:blueberry_pie_slice", "default:blueberry_pie_slice")
minetest.register_alias("bushes:blackberry_pie_slice", "default:blackberry_pie_slice")
minetest.register_alias("bushes:raspberry_pie_slice", "default:raspberry_pie_slice")
minetest.register_alias("bushes:strawberry_pie_slice", "default:strawberry_pie_slice")
minetest.register_alias("bushes:gooseberry_pie_slice", "default:gooseberry_pie_slice")
minetest.register_alias("bushes:mixed_berry_pie_slice", "default:mixed_berry_pie_slice")

--classic
minetest.register_alias("bushes_classic:blueberry", "default:blueberry")
minetest.register_alias("bushes_classic:blackberry", "default:blackberry")
minetest.register_alias("bushes_classic:raspberry", "default:raspberry")
minetest.register_alias("bushes_classic:strawberry", "default:strawberry")
minetest.register_alias("bushes_classic:gooseberry", "default:gooseberry")
minetest.register_alias("bushes_classic:mixed_berry", "default:mixed_berry")

minetest.register_alias("bushes_classic:fruitless_bush", "default:fruitless_bush")
minetest.register_alias("bushes_classic:blueberry_bush", "default:blueberry_bush")
minetest.register_alias("bushes_classic:blackberry_bush", "default:blackberry_bush")
minetest.register_alias("bushes_classic:raspberry_bush", "default:raspberry_bush")
minetest.register_alias("bushes_classic:strawberry_bush", "default:strawberry_bush")
minetest.register_alias("bushes_classic:gooseberry_bush", "default:gooseberry_bush")
minetest.register_alias("bushes_classic:mixed_berry_bush", "default:mixed_berry_bush")

minetest.register_alias("bushes_classic:basket_blueberry", "default:basket_blueberry")
minetest.register_alias("bushes_classic:basket_blackberry", "default:basket_blackberry")
minetest.register_alias("bushes_classic:basket_raspberry", "default:basket_raspberry")
minetest.register_alias("bushes_classic:basket_strawberry", "default:basket_strawberry")
minetest.register_alias("bushes_classic:basket_gooseberry", "default:basket_gooseberry")
minetest.register_alias("bushes_classic:basket_mixed_berry", "default:basket_mixed_berry")

minetest.register_alias("bushes_classic:blueberry_pie_raw", "default:blueberry_pie_raw")
minetest.register_alias("bushes_classic:blackberry_pie_raw", "default:blackberry_pie_raw")
minetest.register_alias("bushes_classic:raspberry_pie_raw", "default:raspberry_pie_raw")
minetest.register_alias("bushes_classic:strawberry_pie_raw", "default:strawberry_pie_raw")
minetest.register_alias("bushes_classic:gooseberry_pie_raw", "default:gooseberry_pie_raw")
minetest.register_alias("bushes_classic:mixed_berry_pie_raw", "default:mixed_berry_pie_raw")

minetest.register_alias("bushes_classic:blueberry_pie_cooked", "default:blueberry_pie_cooked")
minetest.register_alias("bushes_classic:blackberry_pie_cooked", "default:blackberry_pie_cooked")
minetest.register_alias("bushes_classic:raspberry_pie_cooked", "default:raspberry_pie_cooked")
minetest.register_alias("bushes_classic:strawberry_pie_cooked", "default:strawberry_pie_cooked")
minetest.register_alias("bushes_classic:gooseberry_pie_cooked", "default:gooseberry_pie_cooked")
minetest.register_alias("bushes_classic:mixed_berry_pie_cooked", "default:mixed_berry_pie_cooked")

minetest.register_alias("bushes_classic:blueberry_pie_slice", "default:blueberry_pie_slice")
minetest.register_alias("bushes_classic:blackberry_pie_slice", "default:blackberry_pie_slice")
minetest.register_alias("bushes_classic:raspberry_pie_slice", "default:raspberry_pie_slice")
minetest.register_alias("bushes_classic:strawberry_pie_slice", "default:strawberry_pie_slice")
minetest.register_alias("bushes_classic:gooseberry_pie_slice", "default:gooseberry_pie_slice")
minetest.register_alias("bushes_classic:mixed_berry_pie_slice", "default:mixed_berry_pie_slice")

--D R Y P L A N T S--
minetest.register_alias("dryplants:sickle", "default:sickle")
minetest.register_alias("dryplants:grass", "default:grass")
minetest.register_alias("dryplants:hay", "default:hay")
minetest.register_alias("dryplants:grass_short", "default:grass_short")
minetest.register_alias("dryplants:wetreed", "default:wetreed")
minetest.register_alias("dryplants:reed", "default:reed")
minetest.register_alias("dryplants:reedmace_water", "default:reedmace_water")
minetest.register_alias("dryplants:reedmace_water_entity", "default:reedmace_water_entity")
minetest.register_alias("dryplants:reedmace_sapling", "default:reedmace_sapling")
minetest.register_alias("dryplants:reedmace", "default:reedmace")
minetest.register_alias("dryplants:reedmace_height_2", "default:reedmace_height_2")
minetest.register_alias("dryplants:reedmace_height_3", "default:reedmace_height_3")
minetest.register_alias("dryplants:reedmace_height_3_spikes", "default:reedmace_height_3_spikes")
minetest.register_alias("dryplants:reedmace_spikes", "default:reedmace_spikes")
minetest.register_alias("dryplants:reedmace_top", "default:reedmace_top")
minetest.register_alias("dryplants:reedmace_bottom", "default:reedmace_bottom")

minetest.register_alias("dryplants:wetreed_slab", "default:wetreed_slab")
minetest.register_alias("dryplants:reed_slab", "default:reed_slab")
minetest.register_alias("dryplants:wetreed_roof", "default:wetreed_roof")
minetest.register_alias("dryplants:reed_roof", "default:reed_roof")
minetest.register_alias("dryplants:wetreed_roof_corner", "default:wetreed_roof_corner")
minetest.register_alias("dryplants:reed_roof_corner", "default:reed_roof_corner")

minetest.register_alias("dryplants:dandelion_leave", "default:dandelion_leave")
minetest.register_alias("dryplants:juncus", "default:juncus")
minetest.register_alias("dryplants:juncus_02", "default:juncus_02")

minetest.register_alias("dryplants:reedmace_water_entity", "default:reedmace_water_entity")




--F E R N S--
minetest.register_alias("ferns:horsetail_01", "default:horsetail_01")
minetest.register_alias("ferns:horsetail_02", "default:horsetail_02")
minetest.register_alias("ferns:horsetail_03", "default:horsetail_03")
minetest.register_alias("ferns:horsetail_04", "default:horsetail_04")
minetest.register_alias("ferns:fern_01", "default:fern_01")
minetest.register_alias("ferns:fern_02", "default:fern_02")
minetest.register_alias("ferns:fern_03", "default:fern_03")
minetest.register_alias("ferns:tree_fern_leaves", "default:tree_fern_leaves")
minetest.register_alias("ferns:tree_fern_leaves_02", "default:tree_fern_leaves_02")
minetest.register_alias("ferns:fern_trunk", "default:fern_trunk")
minetest.register_alias("ferns:sapling_tree_fern", "default:sapling_tree_fern")

minetest.register_alias("ferns:fiddlehead", "default:fiddlehead")
minetest.register_alias("ferns:fiddlehead_roasted", "default:fiddlehead_roasted")
minetest.register_alias("ferns:ferntuber", "default:ferntuber")
minetest.register_alias("ferns:ferntuber_roasted", "default:ferntuber_roasted")

minetest.register_alias("ferns:fern_trunk_big", "default:fern_trunk_big")
minetest.register_alias("ferns:fern_trunk_big_top", "default:fern_trunk_big_top")
minetest.register_alias("ferns:tree_fern_leaves_giant", "default:tree_fern_leaves_giant")
minetest.register_alias("ferns:tree_fern_leave_big", "default:tree_fern_leave_big")
minetest.register_alias("ferns:tree_fern_leave_big_end", "default:tree_fern_leave_big_end")
minetest.register_alias("ferns:sapling_giant_tree_fern", "default:sapling_giant_tree_fern")

--F L O W E R S--


--M O L E H I L L S--
minetest.register_alias("molehills:molehill", "default:molehill")

--N A T U R E   C L A S S I C--
minetest.register_alias("nature:blossom", "default:blossom")

--P O I S O N I V Y--
minetest.register_alias("poisonivy:seedling", "default:seedling")
minetest.register_alias("poisonivy:sproutling", "default:sproutling")
minetest.register_alias("poisonivy:climbing", "default:climbing")

--T R U N K S--
minetest.register_alias("trunks:twig_1", "default:twig_1")
minetest.register_alias("trunks:twig_2", "default:twig_2")
minetest.register_alias("trunks:twig_3", "default:twig_3")
minetest.register_alias("trunks:twig_4", "default:twig_4")
minetest.register_alias("trunks:twig_5", "default:twig_5")
minetest.register_alias("trunks:twig_6", "default:twig_6")
minetest.register_alias("trunks:twig_7", "default:twig_7")
minetest.register_alias("trunks:twig_8", "default:twig_8")
minetest.register_alias("trunks:twig_9", "default:twig_9")
minetest.register_alias("trunks:twig_10", "default:twig_10")
minetest.register_alias("trunks:twig_11", "default:twig_11")
minetest.register_alias("trunks:twig_12", "default:twig_12")
minetest.register_alias("trunks:twig_13", "default:twig_13")
minetest.register_alias("trunks:twig_14", "default:twig_14")
minetest.register_alias("trunks:twig_15", "default:twig_15")
minetest.register_alias("trunks:twig_16", "default:twig_16")
minetest.register_alias("trunks:twig_17", "default:twig_17")
minetest.register_alias("trunks:twig_18", "default:twig_18")
minetest.register_alias("trunks:twig_19", "default:twig_19")
minetest.register_alias("trunks:twigs", "default:twigs")
minetest.register_alias("trunks:treeroot", "default:treeroot")

minetest.register_alias("trunks:twigs_slab", "default:twigs_slab")
minetest.register_alias("trunks:twigs_roof", "default:twigs_roof")
minetest.register_alias("trunks:twigs_roof_corner", "default:twigs_roof_corner")
minetest.register_alias("trunks:twigs_roof_corner_2", "default:twigs_roof_corner_2")
minetest.register_alias("trunks:twigs_roof_corner_2", "default:twigs_roof_corner_2")

minetest.register_alias("trunks:moss", "default:moss")
minetest.register_alias("trunks:moss_fungus", "default:moss_fungus")

--V I N E S--
minetest.register_alias("vines:rope_block", "default:rope_block")
minetest.register_alias("vines:vine", "default:vine")
minetest.register_alias("vines:vine_middle", "default:vine_middle")
minetest.register_alias("vines:vine_end", "default:vine_end")
minetest.register_alias("vines:vine_rotten", "default:vine_rotten")
minetest.register_alias("vines:rope", "default:rope")
minetest.register_alias("vines:rope_end", "default:rope_end")
minetest.register_alias("vines:shears", "default:tool_shears")

minetest.register_alias("vines:root", "default:root")
minetest.register_alias("vines:root_middle", "default:root_middle")
minetest.register_alias("vines:root_end", "default:root_end")

minetest.register_alias("vines:jungle", "default:jungle")
minetest.register_alias("vines:jungle_middle", "default:jungle_middle")
minetest.register_alias("vines:jungle_end", "default:jungle_end")

minetest.register_alias("vines:side", "default:side")
minetest.register_alias("vines:side_middle", "default:side_middle")
minetest.register_alias("vines:side_end", "default:side_end")

minetest.register_alias("vines:willow", "default:willow")
minetest.register_alias("vines:willow_middle", "default:willow_middle")
minetest.register_alias("vines:willow_end", "default:willow_end")


--W O O D S O I L S--
minetest.register_alias("woodsoils:grass_with_leaves_1", "default:grass_with_leaves_1")
minetest.register_alias("woodsoils:grass_with_leaves_2", "default:grass_with_leaves_2")
minetest.register_alias("woodsoils:dirt_with_leaves_1", "default:dirt_with_leaves_1")
minetest.register_alias("woodsoils:dirt_with_leaves_2", "default:dirt_with_leaves_2")

--Y O U N G T R E E S--
minetest.register_alias("youngtrees:bamboo", "default:bamboo")
minetest.register_alias("youngtrees:youngtree2_middle", "default:youngtree2_middle")
minetest.register_alias("youngtrees:youngtree_top", "default:youngtree_top")
minetest.register_alias("youngtrees:youngtree_middle", "default:youngtree_middle")
minetest.register_alias("youngtrees:youngtree_bottom", "default:youngtree_bottom")
minetest.register_alias("youngtrees:youngtree_bottom", "default:youngtree_bottom")















